import pytest
"""Role testing files using testinfra."""

@pytest.mark.parametrize('package', [
    'docker-ce',
    'docker-ce-cli',
    'containerd.io' 
])
def test_packages(host, package):
    pkg = host.package(package)
    assert pkg.is_installed

def test_group(host):
    grp = host.user("user").groups
    assert "docker" in grp

def test_file_exist(host):
    f = host.file("/etc/docker/daemon.json")
    assert f.exists
    registries = ["test.registry", "test2.registry"]
    for registry in registries:
        assert registry in f.content_string